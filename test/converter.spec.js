// TDD - Test Driven Development - Unit Testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe('Color code converter', () => {
    describe('RGB to Hex conversion', () => {
        it('converts the basic colors', () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    }),
    // omayritys 1 
    /*describe('Hex to RGB conversion', () => {
        it('converts the color to RGB', () => {
            const hexadecimal = converter.hexToRgb('ff0000')
            /*const r = converter.hexToRgb('ff');
            const g = converter.hexToRgb('00');
            const b = converter.hexToRgb('00');

            expect(hexadecimal).to.equal('255,0,0');
        })
    }) */
    
    // 
    // omayritys 2
    describe('Hex to RGB conversion', () => {
        it('converts the color to RGB', () => {
            const redRgb = converter.hexToRgb('ff0000'.match(/.{1,2}/g)); //255,0,0
            expect(redRgb).to.deep.equal([255,0,0]);
        });
    });

    //
});