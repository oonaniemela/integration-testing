const pad = (hex) => {
    return(hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // ff, 02
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex) // "ff0000"
    },
    // omayritys 1
    /*hexToRgb: (hexadecimal) => {
        const bigInt = parseInt(hexadecimal, 16);
        const r = (bigInt >> 16) & 255;
        const g = (bigInt >> 8) & 255;
        const b = bigInt & 255;
    
        return r + "," + g + "," + b;
    }*/
    //
    // omayritys 2

    hexToRgb: (hexadecimal) => {
        let rgbValues = [parseInt(hexadecimal[0], 16), parseInt(hexadecimal[1], 16), parseInt(hexadecimal[2], 16)];
        return rgbValues;
    }
    //
}